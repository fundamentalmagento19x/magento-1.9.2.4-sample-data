# README #

* git clone https://prscreative@bitbucket.org/fundamentalmagento19x/magento-1.9.2.4-sample-data.git

# Setup Environment
* 1.create database 
* 2.import database from folder DB
* 3.rename repository to "fundamental"
* 4.URL working : "http://127.0.0.1/fundamental"

# MySQL User
* Username: root
* Password: password

# Import Database by source command line
* "in folder fundamintal"
* ls
* cd DA
* ls { You can see fundamental.sql in list}
* mysql -u root -ppassword
* create database fundamental;
* use fundamental;
* source fundamental.sql;
* working........


# Test
* frontend url:  "http://127.0.0.1/fundamental"
* backend url :  "http://127.0.0.1/fundamental/admin"

# Admin panel User
* username: admin
* password: password1

# Build in
* Magento 1.9.2.4
* Install Sample Data

# System Require
* PHP 5.5.x - 5.6.x | not support php 7.0.x
* MySQL 5.5 - current
* Apache2.4.x
* phpmyadmin | adminer | tools etc.